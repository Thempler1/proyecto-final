const mongoose = require('mongoose');
const { Schema } = mongoose;

const ReserveSchema = new Schema ({
    name: {type: String, required: true},
    suite: {type: String, required: true},
    dateArrive: {type: String, required: true},
    dateLeave: {type: String, required: true}
});

module.exports = mongoose.model('Reserve', ReserveSchema);