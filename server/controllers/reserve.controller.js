const Reserve = require('../models/reserve'); //modelo de datos

const reserveCtrl = {};

reserveCtrl.getReserves = async (req, res) => {
    const reserves = await Reserve.find();
    res.json(reserves);
};

reserveCtrl.createReserve = async (req, res) => {
    const reserve = new Reserve({
        name: req.body.name,
        suite: req.body.suite,
        dateArrive: req.body.dateArrive,
        dateLeave: req.body.dateLeave
    });
    await reserve.save();
    res.json({
        'status': 'Reserve Saved'
    });
};

reserveCtrl.getReserve = async (req, res) => {
    const reserve = await Reserve.findById(req.params.id);
    res.json(reserve);
};

reserveCtrl.editReserve = async (req, res) => {
    const { id } = req.params;
    const reserve = {
        name: req.body.name,
        suite: req.body.suite,
        dateArrive: req.body.dateArrive,
        dateLeave: req.body.dateLeave
    }
    await Reserve.findByIdAndUpdate (id, {$set: reserve}, {new: true});
    res.json({'status': 'Reserve Updated'});
};

reserveCtrl.deleteReserve = async (req, res) => {
    await Reserve.findByIdAndRemove(req.params.id);
    res.json({'status': 'Reserve Deleted'});
};

module.exports = reserveCtrl;