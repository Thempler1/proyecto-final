import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Reserve } from '../models/reserve';
import { BookingComponent } from '../features/booking/booking.component';

@Injectable({
  providedIn: 'root'
})
export class ReserveService {

  selectedReserve: Reserve;
  reserves: Reserve[];
  readonly URL_API = 'http://localhost:3000/api/reserves';

  constructor(private http: HttpClient) { 
    this.selectedReserve =  new Reserve();
  }

  getReserves(){
    return this.http.get(this.URL_API);
  }

  postReserve(reserve: Reserve){
    return this.http.post(this.URL_API, reserve);
  }

  putReserve(reserve: Reserve){
    return this.http.put(this.URL_API + `/${reserve._id}`, reserve);
  }

  deleteReserve(_id: string){
    return this.http.delete(this.URL_API+ `/${_id}`);
  }

}
