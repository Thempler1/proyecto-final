export class Reserve {

    constructor(
        _id = '',
        name = '',
        suite = '',
        dateArrive = '',
        dateLeave = '',
    ){
        this._id=_id;
        this.name=name;
        this.suite=suite;
        this.dateArrive=dateArrive;
        this.dateLeave=dateLeave;
    }

    _id: string;
    name: string;
    suite: string;
    dateArrive: string;
    dateLeave: string;
}
