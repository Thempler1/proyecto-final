import { Component, OnInit } from '@angular/core';

import { ReserveService } from '../../services/reserve.service';
import { NgForm } from '@angular/forms';
import { Reserve } from 'src/app/models/reserve';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
  providers: [ReserveService]
})
export class BookingComponent implements OnInit {

  constructor( private reserveService: ReserveService) { }

  ngOnInit() {
    this.getReserves();
  }

  addReserve(form: NgForm){
    if(form.value._id){
      this.reserveService.putReserve(form.value)
      .subscribe(res => {
        this.resetForm(form);
        alert("Reserva editada exitosamente! :O");
        this.getReserves();
      })
    }else{
      this.reserveService.postReserve(form.value)
      .subscribe(res => {
        this.resetForm(form);
        alert("Reserva agendada exitosamente! ;)");
        this.getReserves();
      });
    }
    
  }

  getReserves(){
    this.reserveService.getReserves()
    .subscribe(res => {
      this.reserveService.reserves = res as Reserve[];
      console.log(res);
    });
  }

  editReserve (reserve: Reserve){
    this.reserveService.selectedReserve = reserve;
  }

  deleteReserve(_id: string){
    if(confirm('Estás seguro de eliminar la reserva?')){
      this.reserveService.deleteReserve(_id)
      .subscribe(res => {
        this.getReserves();
        alert('Reserva eliminada');
      });
    }
  }

  resetForm(form?: NgForm){
    if (form){
      form.reset();
      this.reserveService.selectedReserve = new Reserve();
    }
  }

}
