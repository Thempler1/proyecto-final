# Hotel Boutique 🏨

## Descripción 🚀
_El proyecto tiene como fin desarrollar una página web para un Hotel Boutique_
El desarollo se efectuó utilizando el stack MEAN, compuesto por MongoDB, Express, Angular y Node.js

## Pre-requisitos 📋
- Instalar los frameworks y softwares utilizados. (Angular, MongoDB,Node.js)
- Clonar o descargar este repositorio.

## Levantamiento de Frontend en Framework Angular💻

Abrir una terminal dentro de la carpeta 'frontend' y ejecutar el siguiente comando:

```
$ ng serve
```
La terminal debe haber comenzado la carga del frontend, esto puede demorar.
Una vez terminado el proceso entrar a [http://localhost:4200/](http://localhost:4200/) Lo que debe mostrar el incio de la web.

## Levantamiento de Backend💻

Dentro de la carpeta raíz del proyecto, ejecutar el siguiente comando

```
$ npm run dev
```
La terminal debe haber comenzado la carga del backend, esto puede demorar.
Una vez terminado el proceso entrar a [http://localhost:3000/api/reserves](http://localhost:3000/api/reserves) Lo que debe mostrar un json con las reservas del hotel.

_NOTA: Si el objeto json retornado es [], no te asustes, es solo que tu base de datos no contiene nuevas reservas._

## Construido con 🛠️
Las siguientes herramientas fueron utilizadas para crear el proyecto

* [Angular](https://angular.io/) - El framework para aplicaciones web, utilizado para el Frontend
* [GitLoad](https://gitlab.com/Thempler1/gitupload/) - Script utilizado para subir archivos al repositorio
* [Nodemon](https://nodemon.io/) - Ayuda a desarrollar aplicaciones basadas en node.js al reiniciar automáticamente la aplicación de node cuando se detectan cambios en los archivos del directorio.
* [MongoDB](https://www.mongodb.com/) - Base de datos no relacional, utilizada para la persistencia de los datos.
* [Express.JS](https://expressjs.com/) - El framework de node.js utilizado para el Backend.
* [Cors.JS](https://www.npmjs.com/package/cors) - Paquete de node.js para proporcionar un middleware Connect / Express.
* [Mongoose.JS](https://mongoosejs.com/) - Mongoose proporciona una solución directa y basada en esquemas para modelar los datos de su aplicación.
* [Postman](https://www.getpostman.com/) - Administrador de las API en Postman, con el entorno de desarrollo de API más completo de la industria.

## Autor ✒️
_Autor del proyecto_
* **Jair Leyton Fuentes** - *Desarrollo Full-Stack* - [Thempler1](https://gitlab.com/Thempler1/) 
